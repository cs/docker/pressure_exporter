from setuptools import setup

setup(
    name="pressure_exporter",
    version="0.2.1",
    description="Prometheus exporter for vacuum pumps & gauges",
    author="Hendrik v. Raven",
    author_email="hendrik@consetetur.de",
    url="https://gitlab.physik.uni-muenchen.de/cs/pressure_exporter.git",
    packages=["pressure_exporter"],
    license="GPL-3",
    install_requires=["aioprometheus[aiohttp]"],
)
