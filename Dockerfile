FROM python:3.9-alpine

COPY . .

RUN pip install -e .

HEALTHCHECK --interval=1m --timeout=3s --retries=5 --start-period=5s \
  CMD /health-check.sh

CMD python3 -m pressure_exporter
