from aioprometheus import Gauge, Summary

pressure_mbar = Gauge("vacuum_pressure_mbar", "Vacuum pressure")

pump_voltage_volts = Gauge("pump_voltage_volts", "Pump voltage")

pump_current_amperes = Gauge("pump_current_amperes", "Pump current")

scrape_time = Summary("vacuum_scrape_time_seconds", "Time to get an update from a pump")
