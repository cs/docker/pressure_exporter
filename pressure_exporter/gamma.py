import asyncio
import logging
import time

from pressure_exporter.metrics import (
    pressure_mbar,
    pump_current_amperes,
    pump_voltage_volts,
    scrape_time,
)

log = logging.getLogger(__name__)


async def get_status(r, w):
    w.write(b"spc 0d\r")
    answer = await r.readuntil(b"\r\n")
    answer = answer.decode("ascii").strip()
    if answer[7:] == "RUNNING":
        status = "running"
    elif answer[7:] == "PUMP ERROR":
        status = "error"
    else:
        status = "stopped"
    return status


async def get_voltage(r, w):
    w.write(b"spc 0c\r")
    answer = await r.readuntil(b"\r\n")
    answer = answer.decode("ascii").strip()
    return float(answer.split(" ")[2])


async def get_current(r, w):
    w.write(b"spc 0a\r")
    answer = await r.readuntil(b"\r\n")
    answer = answer.decode("ascii").strip()
    return float(answer.split(" ")[2])


async def get_pressure(r, w, state):
    if state != "running":
        return 0

    w.write(b"spc 0b\r")
    answer = await r.readuntil(b"\r\n")
    return float(answer.decode("ascii").split(" ")[2])


async def scrape(labels, r, w):
    start = time.time()
    state = await get_status(r, w)
    pump_voltage_volts.set(labels, await get_voltage(r, w))
    pump_current_amperes.set(labels, await get_current(r, w))
    pressure_mbar.set(labels, await get_pressure(r, w, state))
    scrape_time.add(labels, time.time() - start)


async def scrape_gamma(name, host, model):
    labels = dict(type="pump", object=name, model=model)
    r, w = await asyncio.open_connection(host, 23)
    log.info("opened Gamma scraper %s (%s) on %s", name, model, host)

    try:
        while True:
            await scrape(labels, r, w)

            await asyncio.sleep(5)
    finally:
        w.close()
        await w.wait_closed()
        log.info("closed Gamma scraper %s (%s) on %s", name, model, host)


def scrape_functions():
    return (
        (scrape_gamma, ("glass_cell", "gamma-science.cslab", "gamma-10st")),
        (scrape_gamma, ("mot", "gamma-mot.cslab", "gamma-75s")),
        (scrape_gamma, ("oven", "gamma-oven.cslab", "gamma-75s")),
    )
