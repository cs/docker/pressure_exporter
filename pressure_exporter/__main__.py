import asyncio
import logging

from aioprometheus.service import Service

log = logging.getLogger(__name__)

# load the other metrics once the time metric has been loaded. Otherwise there values
# get reported before the scrape function is called, effectively returning the values
# from the last scrape
from pressure_exporter import gamma, mvc3, nextorr


def scrape_functions():
    return (
        *gamma.scrape_functions(),
        *mvc3.scrape_functions(),
        *nextorr.scrape_functions(),
    )


async def run_scrape(fun, args):
    while True:
        try:
            await fun(*args)
        except Exception as e:
            log.exception(e)

        await asyncio.sleep(10)


async def main():
    svr = Service()
    try:
        await svr.start(port=8001)
        await asyncio.gather(*(run_scrape(*scraper) for scraper in scrape_functions()))
    finally:
        await svr.stop()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    asyncio.run(main())
