import asyncio
import logging
import time

from pressure_exporter.metrics import (
    pressure_mbar,
    pump_current_amperes,
    pump_voltage_volts,
    scrape_time,
)

log = logging.getLogger(__name__)


async def get_status(r, w):
    w.write(b"TS\r")
    answer = await r.readuntil(b"\r")
    answer = answer.decode("ascii").strip().split(", ")
    if answer[4].split(" ")[1] == "ON":
        state = "error"
    elif answer[0].split(" ")[1] == "ON":
        state = "running"
    else:
        state = "stopped"
    return state


async def get_voltage(r, w):
    w.write(b"u\r")
    answer = await r.readuntil(b"\r")
    answer = answer.decode("ascii").strip().split(", ")[0]
    return int(answer, base=16)


async def get_current(r, w):
    w.write(b"i\r")
    answer = await r.readuntil(b"\r")
    answer = answer.decode("ascii").strip().split(", ")[0]
    # answer is in hex, convert to dec
    answer = int(answer, base=16)
    if answer & 0x4000:
        mult = 0.1e-6
    elif answer & 0x8000:
        mult = 10e-6
    else:
        mult = 1e-9
    return (answer & ~0xC000) * mult


async def get_pressure(r, w, state):
    w.write(b"Tb\r")
    answer = await r.readuntil(b"\r")
    answer = answer.decode("ascii")
    if not float(answer) and state == "running":
        answer = 1e-10
    return float(answer)


async def scrape(labels, r, w):
    start = time.time()
    state = await get_status(r, w)
    pump_voltage_volts.set(labels, await get_voltage(r, w))
    pump_current_amperes.set(labels, await get_current(r, w))
    pressure_mbar.set(labels, await get_pressure(r, w, state))
    scrape_time.add(labels, time.time() - start)


async def scrape_nextorr(name, host, port):
    labels = dict(type="pump", object=name, model="nextorr-d-200-5")
    r, w = await asyncio.open_connection(host, port)
    log.info("opened Nextorr scraper %s on %s:%s", name, host, port)
    try:
        while True:
            await scrape(labels, r, w)

            await asyncio.sleep(5)
    finally:
        w.close()
        await w.wait_closed()
        log.info("closed Nextorr scraper %s on %s:%s", name, host, port)


def scrape_functions():
    return (
        (scrape_nextorr, ("rubidium", "nextorr-rubidium.cslab", 5000)),
        (scrape_nextorr, ("zeeman-window", "nextorr-zeeman-window.cslab", 5000)),
    )
