import asyncio
import logging
import time

from pressure_exporter.metrics import pressure_mbar, scrape_time

log = logging.getLogger(__name__)


async def scrape(labels, reader, writer):
    start = time.time()
    writer.write(b"RPV1\r")
    read = await reader.readuntil(b"\r")
    answer = float(read.decode("ascii").split("\t")[1].strip())
    pressure_mbar.set(labels, answer)
    scrape_time.add(labels, time.time() - start)


async def scrape_host(name, host, port):
    reader, writer = await asyncio.open_connection(host, port)
    log.info("opened MVC3 scraper %s on %s:%s", name, host, port)
    labels = dict(type="gauge", object=name, model="vacom-mvc-3")
    try:
        while True:
            await scrape(labels, reader, writer)

            await asyncio.sleep(5)
    finally:
        writer.close()
        await writer.wait_closed()
        log.info("closed MVC3 scraper %s on %s:%s", name, host, port)


def scrape_functions():
    return (
        (scrape_host, ("oven", "mvc3-oven.cslab", 5000)),
        (scrape_host, ("mot", "mvc3-mot.cslab", 5000)),
    )
